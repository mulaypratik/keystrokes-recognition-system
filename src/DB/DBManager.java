package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JLabel;

import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class DBManager {

	private Connection conToDB = null;
	private Statement stmtSelect = null, stmtUpdate = null;
	private ResultSet resultSet = null;
	private String strDBUrl = "", strDBDriver = "", strDBUserName = "", strDBPassword = "", strQueryResult = "";
	private PreparedStatement pstmt;

	private JLabel lblDBConnectionStatus;

	private ThreadDBConnector threadDBConnector = null;

	public DBManager(String DBServerIP, String DBPortNo, String DBUserName, String DBPassword, String DBName) {
		System.out.println("In constructor fn.");

		// MSSQL
		strDBUrl = "jdbc:jtds:sqlserver://" + DBServerIP + ":" + DBPortNo + "/" + DBName + ";instance=SQLEXPRESS";

		strDBDriver = "net.sourceforge.jtds.jdbc.Driver";

		strDBUserName = DBUserName;
		strDBPassword = DBPassword;
	}

	public boolean connectToDB(JLabel lblStatus) {
		System.out.println("In connect fn.");

		lblDBConnectionStatus = lblStatus;

		// Don't use ProgressThread here! It'll shoot up the processor with
		// excess load of recursive threading.
		if (lblDBConnectionStatus != null)
			lblDBConnectionStatus.setText("    Connecting to database server...");

		try {
			Class.forName(strDBDriver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		try {
			conToDB = DriverManager.getConnection(strDBUrl, strDBUserName, strDBPassword);
		} catch (SQLException e) {
			e.printStackTrace();

			System.out.println("Reconnecting!");

			return false;
		}

		System.out.println("Connected!");

		if (lblDBConnectionStatus != null)
			lblDBConnectionStatus.setText("    Connection established!");

		ConstantsClass.dbManager = this;

		return true;
	}

	public String getStringFromDB(String Query) {
		strQueryResult = "";

		try {
			stmtSelect = conToDB.createStatement();
			resultSet = stmtSelect.executeQuery(Query);
			resultSet.next();

			if (resultSet.getRow() != 0)
				strQueryResult = resultSet.getString(1);

			resultSet.close();
			stmtSelect.close();
		} catch (SQLException e) {

			System.out.println("Excp type: " + e.getSQLState());
			if (e.getSQLState().equals("HY010") || e.getSQLState().startsWith("08")) {
				System.out.println("Conn error!");

				System.out.println("getStrngFn: " + DBConnectSemaphore.boolIsThreadRunning);
				if (!DBConnectSemaphore.boolIsThreadRunning) {
					DBConnectSemaphore.boolIsThreadRunning = true;
					threadDBConnector = new ThreadDBConnector(this, lblDBConnectionStatus);
					threadDBConnector.start();
				}
			}

			return e.getMessage();
		}

		return strQueryResult;
	}

	public ResultSet getDataFromDB(String Query) {
		resultSet = null;

		try {
			pstmt = conToDB.prepareStatement(Query);
			resultSet = pstmt.executeQuery();
		} catch (SQLException e) {

			if (e.getSQLState().equals("HY010") || e.getSQLState().startsWith("08")) {
				System.out.println("Conn error!");

				if (!DBConnectSemaphore.boolIsThreadRunning) {
					threadDBConnector = new ThreadDBConnector(this, lblDBConnectionStatus);
					threadDBConnector.start();
				}
			}

			return null;
		}

		return resultSet;
	}

	public String executeQuery(String Query) {
		try {
			stmtUpdate = conToDB.createStatement();

			stmtUpdate.executeUpdate(Query);

			stmtUpdate.close();
		} catch (SQLException e) {

			if (e.getSQLState().equals("HY010") || e.getSQLState().startsWith("08")) {
				System.out.println("Conn error!");

				System.out.println("execQFn: " + DBConnectSemaphore.boolIsThreadRunning);
				if (!DBConnectSemaphore.boolIsThreadRunning) {
					DBConnectSemaphore.boolIsThreadRunning = true;
					threadDBConnector = new ThreadDBConnector(this, lblDBConnectionStatus);
					threadDBConnector.start();
				}
			}

			return e.getMessage();
		}

		return null;
	}

	public void closeConnection() {
		if (conToDB != null) {
			try {
				conToDB.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
