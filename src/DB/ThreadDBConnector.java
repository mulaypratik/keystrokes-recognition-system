package DB;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class ThreadDBConnector extends Thread {

	private DBManager manager = null;
	private JLabel lblDBConnectionStatus = null;

	public ThreadDBConnector(DBManager dbManager, JLabel lblStatus) {
		manager = dbManager;
		lblDBConnectionStatus = lblStatus;
	}

	public void run() {
		System.out.println("Run start!");

		manager.closeConnection();

		while (!manager.connectToDB(lblDBConnectionStatus)) {
			System.out.println("Loop!");

			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}

		}

		DBConnectSemaphore.boolIsThreadRunning = false;
		System.out.println("Run end!");
	}

}
