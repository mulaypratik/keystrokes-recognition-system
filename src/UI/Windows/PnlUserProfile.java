package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import BL.Functions.BusinessFunctions;
import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlUserProfile extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JDesktopPane jdpMainFrame;
	private JPanel pnlButtonContainer, pnlUserDetails;
	private JLabel lblStatusBar, lblUserIDLbl, lblUserID, lblUserNameLbl, lblUserName, lblUserRoleLbl, lblUserRole;
	private JButton btnLogout;

	private BusinessFunctions businessFunctions = null;

	public PnlUserProfile(BusinessFunctions bFunctions, final JLabel StatusBar, JDesktopPane jdp) {
		// Initialize local variables
		businessFunctions = bFunctions;

		lblStatusBar = StatusBar;
		jdpMainFrame = jdp;

		// Panel properties
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout());

		// Panels
		pnlButtonContainer = new JPanel();
		pnlButtonContainer.setLayout(new BorderLayout());
		pnlButtonContainer.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		pnlUserDetails = new JPanel();
		pnlUserDetails.setLayout(new GridLayout(3, 2, 2, 2));

		// Labels
		lblUserIDLbl = new JLabel("    User ID:");
		lblUserID = new JLabel();
		lblUserNameLbl = new JLabel("    User Name:");
		lblUserName = new JLabel();
		lblUserRoleLbl = new JLabel("    Role:");
		lblUserRole = new JLabel();

		// Buttons
		btnLogout = new JButton("Logout");
		btnLogout.setMnemonic('L');
		btnLogout.setIcon(new ImageIcon("res\\Icons\\logout.png"));
		btnLogout.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				logoutOperation();
			}
		});

		// Adding components
		pnlButtonContainer.add(btnLogout, BorderLayout.EAST);

		pnlUserDetails.add(lblUserIDLbl);
		pnlUserDetails.add(lblUserID);
		pnlUserDetails.add(lblUserNameLbl);
		pnlUserDetails.add(lblUserName);
		pnlUserDetails.add(lblUserRoleLbl);
		pnlUserDetails.add(lblUserRole);

		add(pnlButtonContainer, BorderLayout.NORTH);
		add(pnlUserDetails, BorderLayout.CENTER);

		// Panel properties
		setVisible(true);

		// Fill Data to Controls
		fillDataToControls(ConstantsClass.propertyFileHandler.readPropertyFile("UserIDLoggedIn"));
	}

	private void fillDataToControls(String UserID) {
		lblUserID.setText(UserID);
		lblUserName.setText(ConstantsClass.dbManager
				.getStringFromDB("SELECT DISTINCT User_Name FROM Tbl_UserLoginData WHERE User_ID = '" + UserID + "'"));
		lblUserRole.setText(ConstantsClass.dbManager
				.getStringFromDB("SELECT DISTINCT User_Role FROM Tbl_UserLoginData WHERE User_ID = '" + UserID + "'"));
	}

	private void logoutOperation() {
		int intClosingOption = JOptionPane.showConfirmDialog(null, "Do you want to Logout?", "Logout warning!",
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		if (intClosingOption == JOptionPane.YES_OPTION) {
			businessFunctions.logoutUser();

			JOptionPane.showMessageDialog(null, "You have been successfully logout!", "Information Message",
					JOptionPane.INFORMATION_MESSAGE);

			lblStatusBar.setText("    Successfully logout!");

			jdpMainFrame.removeAll();
		}
	}

}
