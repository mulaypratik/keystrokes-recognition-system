package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;

import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlUserAccountMgmt extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTabbedPane tbpAccMgmt;
	private JPanel pnlUserAccessRights, pnlUserDelete, pnlAccessRightsComponentsContainer, pnlRbUserRoleContainer,
			pnlUserDeleteComponentsContainer, pnlSaveBtnContainer, pnlDeleteBtnContainer;
	private JComboBox<String> comboUserIDAccessRights, comboUserIDUserDelete;
	private ButtonGroup bgUserRole;
	private JRadioButton rbUserRoleAdmin, rbUserRoleUser;
	private JLabel lblUserIDAccessRights, lblUserRole, lblUserIDUserDelete;
	private JButton btnSaveUserIDAccessRights, btnDeleteUser;

	private int intDeletionOption = 0;
	private String strErrMsg = "";
	private ResultSet rsUserID = null;

	public PnlUserAccountMgmt() {
		// TabbedPanes
		tbpAccMgmt = new JTabbedPane();

		// Panels
		pnlUserAccessRights = new JPanel();
		pnlUserAccessRights.setLayout(new BorderLayout());

		pnlUserDelete = new JPanel();
		pnlUserDelete.setLayout(new BorderLayout());

		pnlAccessRightsComponentsContainer = new JPanel();
		pnlAccessRightsComponentsContainer.setLayout(new GridLayout(2, 2, 2, 2));

		pnlRbUserRoleContainer = new JPanel();
		pnlRbUserRoleContainer.setLayout(new GridLayout(1, 2, 2, 2));
		pnlRbUserRoleContainer.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		pnlUserDeleteComponentsContainer = new JPanel();
		pnlUserDeleteComponentsContainer.setLayout(new GridLayout(1, 2, 2, 2));

		pnlSaveBtnContainer = new JPanel();
		pnlSaveBtnContainer.setLayout(new FlowLayout());

		pnlDeleteBtnContainer = new JPanel();
		pnlDeleteBtnContainer.setLayout(new FlowLayout());

		// Labels
		lblUserIDAccessRights = new JLabel("    Select User ID:");
		lblUserRole = new JLabel("    Select User Role:");
		lblUserIDUserDelete = new JLabel("    Select User ID:");

		// ComboBoxes
		comboUserIDAccessRights = new JComboBox<>();
		comboUserIDAccessRights.addItem("-Please Select-");
		comboUserIDAccessRights.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboUserIDAccessRights.getSelectedIndex() == 0)
					btnSaveUserIDAccessRights.setEnabled(false);
				else {
					btnSaveUserIDAccessRights.setEnabled(true);

					if (comboUserIDAccessRights.getSelectedItem() != null)
						if (ConstantsClass.dbManager
								.getStringFromDB("SELECT DISTINCT User_Role FROM Tbl_UserLoginData WHERE User_ID = '"
										+ comboUserIDAccessRights.getSelectedItem().toString() + "'")
								.equals("Admin"))
							rbUserRoleAdmin.setSelected(true);
						else
							rbUserRoleUser.setSelected(true);
				}
			}
		});

		comboUserIDUserDelete = new JComboBox<>();
		comboUserIDUserDelete.addItem("-Please Select-");
		comboUserIDUserDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboUserIDUserDelete.getSelectedIndex() == 0)
					btnDeleteUser.setEnabled(false);
				else
					btnDeleteUser.setEnabled(true);
			}
		});

		// ButtonGroups
		bgUserRole = new ButtonGroup();

		// RadioButtons
		rbUserRoleUser = new JRadioButton("User");
		rbUserRoleUser.setSelected(true);

		rbUserRoleAdmin = new JRadioButton("Admin");

		// Buttons
		btnSaveUserIDAccessRights = new JButton("Save Changes");
		btnSaveUserIDAccessRights.setIcon(new ImageIcon("res\\Icons\\Save.png"));
		btnSaveUserIDAccessRights.setMnemonic('S');
		btnSaveUserIDAccessRights.setEnabled(false);
		btnSaveUserIDAccessRights.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				strErrMsg = ConstantsClass.dbManager.executeQuery("UPDATE Tbl_UserLoginData SET User_Role = '"
						+ (rbUserRoleAdmin.isSelected() ? rbUserRoleAdmin.getText() : rbUserRoleUser.getText())
						+ "' WHERE User_ID = '" + comboUserIDAccessRights.getSelectedItem().toString() + "'");

				if (strErrMsg == null) {
					JOptionPane.showMessageDialog(null, "User Role changed successfully!", "Information Message",
							JOptionPane.INFORMATION_MESSAGE);

					// Refresh UserID ComboBoxes
					fillComboUserID();
				} else
					JOptionPane.showMessageDialog(null, strErrMsg, "Error Message", JOptionPane.ERROR_MESSAGE);
			}
		});

		btnDeleteUser = new JButton("Delete User");
		btnDeleteUser.setIcon(new ImageIcon("res\\Icons\\Delete.png"));
		btnDeleteUser.setMnemonic('D');
		btnDeleteUser.setEnabled(false);
		btnDeleteUser.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				intDeletionOption = JOptionPane.showConfirmDialog(null, "Delete User?", "Deletion warning!",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if (intDeletionOption == JOptionPane.YES_OPTION) {

					strErrMsg = ConstantsClass.dbManager.executeQuery("DELETE FROM Tbl_UserLoginData WHERE User_ID = '"
							+ comboUserIDUserDelete.getSelectedItem().toString() + "'"); // No need to
																							// separately
																							// delete from
																							// Tbl_UserKeyTimeSeriesDataAvg
																							// as there is a
																							// foreign key
																							// relationship
																							// between both
																							// the tables

					if (strErrMsg == null) {
						JOptionPane.showMessageDialog(null, "User deleted successfully!", "Information Message",
								JOptionPane.INFORMATION_MESSAGE);

						// Refresh UserID ComboBoxes
						fillComboUserID();
					} else
						JOptionPane.showMessageDialog(null, strErrMsg, "Error Message", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout());

		// Adding components
		bgUserRole.add(rbUserRoleUser);
		bgUserRole.add(rbUserRoleAdmin);

		pnlRbUserRoleContainer.add(rbUserRoleUser);
		pnlRbUserRoleContainer.add(rbUserRoleAdmin);

		pnlAccessRightsComponentsContainer.add(lblUserIDAccessRights);
		pnlAccessRightsComponentsContainer.add(comboUserIDAccessRights);
		pnlAccessRightsComponentsContainer.add(lblUserRole);
		pnlAccessRightsComponentsContainer.add(pnlRbUserRoleContainer);

		pnlUserDeleteComponentsContainer.add(lblUserIDUserDelete);
		pnlUserDeleteComponentsContainer.add(comboUserIDUserDelete);

		pnlSaveBtnContainer.add(btnSaveUserIDAccessRights);
		pnlDeleteBtnContainer.add(btnDeleteUser);

		pnlUserAccessRights.add(pnlAccessRightsComponentsContainer, BorderLayout.CENTER);
		pnlUserAccessRights.add(pnlSaveBtnContainer, BorderLayout.SOUTH);

		pnlUserDelete.add(pnlUserDeleteComponentsContainer, BorderLayout.CENTER);
		pnlUserDelete.add(pnlDeleteBtnContainer, BorderLayout.SOUTH);

		tbpAccMgmt.add(pnlUserAccessRights, "Set User Role");
		tbpAccMgmt.add(pnlUserDelete, "Delete User");

		add(tbpAccMgmt);

		// Panel properties
		setVisible(true);

		// Fill UserID ComboBoxes
		fillComboUserID();
	}

	private void fillComboUserID() {
		// Reset RadioButton selection
		rbUserRoleUser.setSelected(true);

		comboUserIDAccessRights.removeAllItems();
		comboUserIDAccessRights.addItem("-Please Select-");

		comboUserIDUserDelete.removeAllItems();
		comboUserIDUserDelete.addItem("-Please Select-");

		rsUserID = ConstantsClass.dbManager.getDataFromDB("SELECT DISTINCT User_ID FROM Tbl_UserLoginData");

		if (rsUserID != null)
			try {
				while (rsUserID.next()) {
					comboUserIDAccessRights.addItem(rsUserID.getString("User_ID"));

					comboUserIDUserDelete.addItem(rsUserID.getString("User_ID"));
				}

			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
			}
	}

}
