package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import BL.Functions.BusinessFunctions;
import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlUserRegistrationWindow extends JPanel implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane, pnlRbUserRoleContainer, pnlPassword;
	private JLabel lblUserID, lblUserName, lblUserRole, lblPassword;
	private JTextField txtUserID, txtUserName;
	private JPasswordField txtPassword;
	private JButton btnClearPassword, btnSave, btnClearAllControls;
	private ButtonGroup bgUserRole;
	private JRadioButton rbUserRoleAdmin, rbUserRoleUser;

	private String strErrMsg = "", strPassword = "";
	int intCountNoOfSamples = 0;

	private BusinessFunctions businessFunctions = null;

	public PnlUserRegistrationWindow(BusinessFunctions bFunctions) {
		// Initialize other class objects
		businessFunctions = bFunctions;

		// Panels
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(5, 2, 2, 2));

		pnlRbUserRoleContainer = new JPanel();
		pnlRbUserRoleContainer.setLayout(new GridLayout(1, 2, 2, 2));
		pnlRbUserRoleContainer.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		pnlPassword = new JPanel();
		pnlPassword.setLayout(new BorderLayout());

		// Labels
		lblUserID = new JLabel("    Enter User ID:");
		lblUserName = new JLabel("    Enter User Name:");
		lblPassword = new JLabel("    Enter your Password:");
		lblUserRole = new JLabel("    Select User Role:");

		// TextFields
		txtUserID = new JTextField(ConstantsClass.dbManager.getStringFromDB(
				"SELECT 'A'+ ISNULL(CONVERT(VARCHAR, (CONVERT(INT, SUBSTRING(MAX(User_ID), 2, MAX(LEN(User_ID)))) + 1)), 200) FROM Tbl_UserLoginData")); // set
		// MAX
		// (User_ID)
		// +
		// 1
		// as
		// next
		// User_ID
		txtUserName = new JTextField();

		// PasswordFields
		txtPassword = new JPasswordField();
		txtPassword.addKeyListener(this);

		// ButtonGroups
		bgUserRole = new ButtonGroup();

		// RadioButtons
		rbUserRoleUser = new JRadioButton("User");
		rbUserRoleUser.setSelected(true);

		rbUserRoleAdmin = new JRadioButton("Admin");

		// Buttons
		btnSave = new JButton("Save");
		btnSave.setIcon(new ImageIcon("res\\Icons\\Save.png"));
		btnSave.setMnemonic('S');
		btnSave.addActionListener(new ActionListener() {

			@Override
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {

				// Validate for blank fields
				if (txtUserID.getText().length() != 0 && txtUserName.getText().length() != 0
						&& txtPassword.getText().length() != 0) {
					if (strPassword.equals("") || txtPassword.getText().equals(strPassword)) {
						strPassword = txtPassword.getText();

						// Taking User's patterns three times
						if (intCountNoOfSamples < 3) {
							intCountNoOfSamples++;
							businessFunctions.getPatternSampleFromUser(intCountNoOfSamples);

							// Show user no. of attempts more for pattern
							// recording.
							if (intCountNoOfSamples != 3)
								JOptionPane.showMessageDialog(null,
										"Enter the same password again " + (3 - intCountNoOfSamples) + " time(s).",
										"Information Message", JOptionPane.INFORMATION_MESSAGE);

							// Reset controls
							txtPassword.setText("");
							txtPassword.requestFocus();
						}

						// Saving average of three samples
						if (intCountNoOfSamples == 3) {
							strErrMsg = businessFunctions
									.saveUserRegistrationDataInDB(txtUserID.getText(), txtUserName.getText(),
											(rbUserRoleAdmin.isSelected() ? rbUserRoleAdmin.getText()
													: rbUserRoleUser.getText()),
											strPassword, "" + strPassword.hashCode());

							if (strErrMsg != null)
								JOptionPane.showMessageDialog(null, strErrMsg, "Error Message",
										JOptionPane.ERROR_MESSAGE);
							else
								JOptionPane.showMessageDialog(null, "Data saved successfuly!", "Information Message",
										JOptionPane.INFORMATION_MESSAGE);

							// Reset controls and variables
							clearControls();
							txtUserID.requestFocus();

							intCountNoOfSamples = 0;
							strPassword = "";
						}
					} else {
						JOptionPane.showMessageDialog(null, "Please enter same password as in prvious attempt.",
								"Error Message", JOptionPane.ERROR_MESSAGE);
						txtPassword.setText("");
						txtPassword.requestFocus();
					}
				} else
					JOptionPane.showMessageDialog(null, "User ID and User Name fields cannot be empty!",
							"Error Message", JOptionPane.ERROR_MESSAGE);
			}
		});

		btnClearPassword = new JButton();
		btnClearPassword.setIcon(new ImageIcon("res\\Icons\\Clear_Password.png"));
		btnClearPassword.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				passwordReset();

				txtPassword.setText("");
				txtPassword.requestFocus();
			}
		});

		btnClearAllControls = new JButton("Clear");
		btnClearAllControls.setIcon(new ImageIcon("res\\Icons\\Clear.png"));
		btnClearAllControls.setMnemonic('C');
		btnClearAllControls.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				passwordReset();

				clearControls();
				txtUserID.requestFocus();
			}
		});

		// Adding components
		bgUserRole.add(rbUserRoleUser);
		bgUserRole.add(rbUserRoleAdmin);

		pnlRbUserRoleContainer.add(rbUserRoleUser);
		pnlRbUserRoleContainer.add(rbUserRoleAdmin);

		pnlPassword.add(txtPassword, BorderLayout.CENTER);
		pnlPassword.add(btnClearPassword, BorderLayout.EAST);

		contentPane.add(lblUserID);
		contentPane.add(txtUserID);
		contentPane.add(lblUserName);
		contentPane.add(txtUserName);
		contentPane.add(lblUserRole);
		contentPane.add(pnlRbUserRoleContainer);
		contentPane.add(lblPassword);
		contentPane.add(pnlPassword);
		contentPane.add(btnSave);
		contentPane.add(btnClearAllControls);

		// Panel properties
		setLayout(new BorderLayout());

		add(contentPane, BorderLayout.CENTER);
		setVisible(true);
	}

	private void passwordReset() {
		businessFunctions.clearPatternList();
		intCountNoOfSamples = 0;
		strPassword = "";
	}

	private void clearControls() {
		txtUserID.setText(ConstantsClass.dbManager.getStringFromDB(
				"SELECT 'A'+ ISNULL(CONVERT(VARCHAR, (CONVERT(INT, SUBSTRING(MAX(User_ID), 2, MAX(LEN(User_ID)))) + 1)), 200) FROM Tbl_UserLoginData"));
		txtUserName.setText("");
		txtPassword.setText("");
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyPressedEvent();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyReleaseEvent();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyTypedEvent();
	}

}
