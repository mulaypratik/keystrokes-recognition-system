package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyVetoException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import BL.Functions.BusinessFunctions;
import BL.ProgramConstants.ConstantsClass;
import BL.SpeechSynthesizer.SpeechThread;
import BL.SpeechSynthesizer.TextToSpeech;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class MainFrame extends JFrame implements WindowListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JMenuBar menuBar;
	private JMenu menuStart, menuSettings, menuHelp;
	private JMenuItem menuItemConnect, menuItemUserLogin, menuItemUserRegistration, menuItemExit,
			menuItemDBServerSettings, menuItemUserManagement, menuItemThresholdSetter, menuItemContactUs;
	private JInternalFrame jifBody;
	private JPanel pnlStatus;
	private JLabel lblStatusBar, lblTradeMark;
	private JDesktopPane jdp;
	private Dimension dimFrame;
	private ImageIcon icBackground;
	private Image imgBackground;

	private String strDBName = null;

	private int intClosingOption = 0;

	private BusinessFunctions businessFunctions = null;
	private TextToSpeech textToSpeech = null;
	private SpeechThread speechThread = null;

	public MainFrame() {
		// Objects Initialization
		lblStatusBar = new JLabel(); // Special case of Label as a StatusBar
		businessFunctions = new BusinessFunctions(lblStatusBar);
		textToSpeech = new TextToSpeech();

		strDBName = ConstantsClass.propertyFileHandler.readPropertyFile("DBName");

		// Setting JFrame properties
		setTitle("Hi-Tech Biometrics - Keystrokes Recognition System");
		setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		// MenuBar
		menuBar = new JMenuBar();

		// Menus
		menuStart = new JMenu("Start");
		menuStart.setIcon(new ImageIcon("res\\Icons\\Start.png"));
		menuStart.setMnemonic('S');
		menuStart.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent arg0) {
				// Set dynamically MenuItem's titles and icons
				if (ConstantsClass.dbManager == null) {
					menuItemConnect.setText("Connect to Server");
					menuItemConnect.setIcon(new ImageIcon("res\\Icons\\Connect_Menu.png"));
				} else {
					menuItemConnect.setText("Disconnect server connection");
					menuItemConnect.setIcon(new ImageIcon("res\\Icons\\Disconnect_Menu.png"));
				}

				if (ConstantsClass.propertyFileHandler.readPropertyFile("UserIDLoggedIn").equals("")) {
					menuItemUserLogin.setText("User Login");
					menuItemUserLogin.setIcon(new ImageIcon("res\\Icons\\Login_Menu.png"));
				} else {
					menuItemUserLogin.setText("My Profile");
					menuItemUserLogin.setIcon(new ImageIcon("res\\Icons\\User_Profile.png"));
				}

				// Set Menus Enabled/Disabled based on User's Role in the system
				menuItemUserRegistration.setEnabled(businessFunctions.isAuthenticUser());
			}

			@Override
			public void menuDeselected(MenuEvent arg0) {
			}

			@Override
			public void menuCanceled(MenuEvent arg0) {
			}
		});

		menuSettings = new JMenu("Settings");
		menuSettings.setIcon(new ImageIcon("res\\Icons\\Settings.png"));
		menuSettings.setMnemonic('T');
		menuSettings.addMenuListener(new MenuListener() {

			@Override
			public void menuSelected(MenuEvent arg0) {
				// Set Menus Enabled/Disabled based on User's Role in the system
				menuItemUserManagement.setEnabled(businessFunctions.isAuthenticUser());

				menuItemThresholdSetter.setEnabled(businessFunctions.isAuthenticUser());
			}

			@Override
			public void menuDeselected(MenuEvent arg0) {
			}

			@Override
			public void menuCanceled(MenuEvent arg0) {
			}
		});

		menuHelp = new JMenu("Help");
		menuHelp.setIcon(new ImageIcon("res\\Icons\\Help.png"));
		menuHelp.setMnemonic('H');

		// Menu Items
		menuItemConnect = new JMenuItem("Connect to Server");
		menuItemConnect.setIcon(new ImageIcon("res\\Icons\\Connect_Menu.png"));
		menuItemConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (ConstantsClass.dbManager == null)
					initInternalFrame("Database Server Settings Panel",
							new PnlDBServerSettings(strDBName, lblStatusBar), 750, 300, "res\\Icons\\Connect_Menu.png",
							false);
				else {
					ConstantsClass.dbManager.closeConnection();
					ConstantsClass.dbManager = null;

					lblStatusBar.setText("    Connection Desconnected!");
					jdp.removeAll();
					jdp.repaint();
				}
			}
		});

		menuItemUserLogin = new JMenuItem("User Login");
		menuItemUserLogin.setIcon(new ImageIcon("res\\Icons\\Login_Menu.png"));
		menuItemUserLogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (ConstantsClass.dbManager != null) {
					if (ConstantsClass.propertyFileHandler.readPropertyFile("UserIDLoggedIn").equals(""))
						initInternalFrame("User login", new PnlUserLoginWindow(businessFunctions, jdp), 750, 300,
								"res\\Icons\\Login_Menu.png", false);
					else
						initInternalFrame("My Profile", new PnlUserProfile(businessFunctions, lblStatusBar, jdp), 500,
								300, "res\\Icons\\User_Profile.png", false);
				} else
					initInternalFrame("Database Server Settings Panel",
							new PnlDBServerSettings(strDBName, lblStatusBar), 750, 300, "res\\Icons\\Connect_Menu.png",
							false);
			}
		});

		menuItemUserRegistration = new JMenuItem("User Registration");
		menuItemUserRegistration.setIcon(new ImageIcon("res\\Icons\\Register.png"));
		menuItemUserRegistration.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (ConstantsClass.dbManager != null)
					initInternalFrame("User Registration", new PnlUserRegistrationWindow(businessFunctions), 750, 300,
							"res\\Icons\\Register.png", false);
				else
					initInternalFrame("Database Server Settings Panel",
							new PnlDBServerSettings(strDBName, lblStatusBar), 750, 300, "res\\Icons\\Connect_Menu.png",
							false);
			}
		});

		menuItemExit = new JMenuItem("Exit");
		menuItemExit.setIcon(new ImageIcon("res\\Icons\\Exit.png"));
		menuItemExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (ConstantsClass.dbManager != null)
					ConstantsClass.dbManager.closeConnection();

				windowClosimgOperation();
			}
		});

		menuItemDBServerSettings = new JMenuItem("Databse Server Settings");
		menuItemDBServerSettings.setIcon(new ImageIcon("res\\Icons\\Database_Settings.png"));
		menuItemDBServerSettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				initInternalFrame("Database Server Settings Panel", new PnlDBServerSettings(strDBName, lblStatusBar),
						750, 300, "res\\Icons\\Connect_Menu.png", false);
			}
		});

		menuItemUserManagement = new JMenuItem("User Account Management");
		menuItemUserManagement.setIcon(new ImageIcon("res\\Icons\\User_Management.png"));
		menuItemUserManagement.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (ConstantsClass.dbManager != null)
					initInternalFrame("User Account Management", new PnlUserAccountMgmt(), 450, 200,
							"res\\Icons\\User_Management.png", false);
				else
					initInternalFrame("Database Server Settings Panel",
							new PnlDBServerSettings(strDBName, lblStatusBar), 750, 300, "res\\Icons\\Connect_Menu.png",
							false);
			}
		});

		menuItemThresholdSetter = new JMenuItem("Set Threshold value");
		menuItemThresholdSetter.setIcon(new ImageIcon("res\\Icons\\Threshold.png"));
		menuItemThresholdSetter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				initInternalFrame("Threshold Value Setting Panel", new PnlSetThresholdValue(lblStatusBar), 450, 200,
						"res\\Icons\\Threshold.png", false);
			}
		});

		menuItemContactUs = new JMenuItem("Contact us");
		menuItemContactUs.setIcon(new ImageIcon("res\\Icons\\Contact_Us.png"));

		// Panels
		pnlStatus = new JPanel();
		pnlStatus.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
		pnlStatus.setLayout(new GridLayout(1, 2));

		// Labels
		lblTradeMark = new JLabel(
				"    Developed by:- Pratik C. Mulay (M. Tech Computer Engineering student of MPSTME, NMIMS)");
		lblTradeMark.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));

		// DesktopPane
		icBackground = new ImageIcon("res/img/Keyboarding.jpg");
		imgBackground = icBackground.getImage();

		jdp = new JDesktopPane() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics grphcs) {
				super.paintComponent(grphcs);
				grphcs.drawImage(imgBackground, 50, 50, null);
			}

			@Override
			public Dimension getPreferredSize() {
				return new Dimension(getWidth(), getHeight());
			}
		};

		// Adding components
		menuStart.add(menuItemConnect);
		menuStart.add(menuItemUserLogin);
		menuStart.add(menuItemUserRegistration);
		menuStart.add(menuItemExit);
		menuSettings.add(menuItemDBServerSettings);
		menuSettings.add(menuItemUserManagement);
		menuSettings.add(menuItemThresholdSetter);
		menuHelp.add(menuItemContactUs);

		menuBar.add(menuStart);
		menuBar.add(menuSettings);
		menuBar.add(menuHelp);

		pnlStatus.add(lblStatusBar);
		pnlStatus.add(lblTradeMark);

		setLayout(new BorderLayout());

		add(menuBar, BorderLayout.NORTH);
		add(jdp, BorderLayout.CENTER);

		add(pnlStatus, BorderLayout.SOUTH);

		// Setting JFrame properties
		setIconImage(Toolkit.getDefaultToolkit().getImage("res\\Icons\\Biometrics.png"));
		addWindowListener(this);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setVisible(true);

		// Text to Speech
		speechThread = new SpeechThread(textToSpeech, "Welcome to Hi-Tech Biometrics - Keystrokes Recognition System!");
		speechThread.start();
	}

	private void initInternalFrame(String FrameTitle, JPanel PanelToBeAdded, int PanelWidth, int PanelHeight,
			String IconIamgePath, boolean setMaximumJIF) {
		if (jifBody != null)
			jifBody.dispose();

		jifBody = new JInternalFrame(FrameTitle, false, true, false, true);
		jifBody.setFrameIcon(new ImageIcon(IconIamgePath));

		// Frame Dimension
		dimFrame = getSize();
		jifBody.setBounds(((dimFrame.width - PanelWidth) / 2), ((dimFrame.height - PanelHeight) / 2), PanelWidth,
				PanelHeight);

		jifBody.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		jifBody.add(PanelToBeAdded);
		jifBody.setVisible(true);

		jdp.add(jifBody);

		try {
			jifBody.setMaximum(setMaximumJIF);
		} catch (PropertyVetoException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void windowClosimgOperation() {
		intClosingOption = JOptionPane.showConfirmDialog(null, "Exit application?", "Exit warning!",
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
		if (intClosingOption == JOptionPane.YES_OPTION) {
			businessFunctions.logoutUser();

			if (ConstantsClass.dbManager != null) {
				ConstantsClass.dbManager.closeConnection();
				ConstantsClass.dbManager = null;
			}

			System.exit(0);
		}
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowClosing(WindowEvent e) {
		windowClosimgOperation();
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}

}
