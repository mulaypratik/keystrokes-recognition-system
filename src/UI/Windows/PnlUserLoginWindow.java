package UI.Windows;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import BL.Functions.BusinessFunctions;
import BL.ProgramConstants.ConstantsClass;
import BL.SpeechSynthesizer.SpeechThread;
import BL.SpeechSynthesizer.TextToSpeech;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlUserLoginWindow extends JPanel implements KeyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JDesktopPane jdpMainFrame;
	private JPanel contentPane;
	private JLabel lblUserID, lblUserName, lblPassword;
	private JButton btnClear, btnLogin;
	private JTextField txtUserName;
	private JPasswordField txtPassword;
	private JComboBox<String> comboUserID;

	private String strPassword = "", strUserPasswordPattern = "";
	private ResultSet rsUserID = null, rsUserData = null;

	private BusinessFunctions businessFunctions = null;
	private TextToSpeech textToSpeech = null;
	private SpeechThread speechThread = null;

	/**
	 * Create the frame.
	 * 
	 * @param bFunctions
	 * @param jdp
	 */

	public PnlUserLoginWindow(BusinessFunctions bFunctions, JDesktopPane jdp) {
		// Initialize local variables
		businessFunctions = bFunctions;
		textToSpeech = new TextToSpeech();

		jdpMainFrame = jdp;

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(4, 2, 2, 2));

		// Labels
		lblUserID = new JLabel("User ID:");
		lblUserName = new JLabel("User Name:");
		lblPassword = new JLabel("Password:");

		// ComboBoxes
		comboUserID = new JComboBox<>();
		comboUserID.addItem("-Please Select-");
		comboUserID.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (comboUserID.getSelectedIndex() == 0) {
					txtUserName.setText("");
					strPassword = "";
					strUserPasswordPattern = "";

					btnLogin.setEnabled(false);
				} else {
					comboUserIDListener();
					btnLogin.setEnabled(true);
				}
			}
		});

		// TextFields
		txtUserName = new JTextField();
		txtUserName.setEditable(false);

		// PasswordField
		txtPassword = new JPasswordField();
		txtPassword.addKeyListener(this);

		// Buttons
		btnClear = new JButton("Clear");
		btnClear.setIcon(new ImageIcon("res\\Icons\\Clear.png"));
		btnClear.setMnemonic('C');
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				businessFunctions.clearPatternList();

				txtPassword.setText("");
				txtPassword.requestFocus();
			}
		});

		btnLogin = new JButton("Login");
		btnLogin.setIcon(new ImageIcon("res\\Icons\\login.png"));
		btnLogin.setEnabled(false);
		btnLogin.setMnemonic('L');
		btnLogin.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {

				if (businessFunctions.PasswordMatch("" + txtPassword.getText().hashCode(), strPassword,
						strUserPasswordPattern,
						Integer.parseInt(
								ConstantsClass.propertyFileHandler.readPropertyFile("ThresholdPercentagePerVector")),
						Integer.parseInt(
								ConstantsClass.propertyFileHandler.readPropertyFile("ThresholdPercentageOverall")))) { // Threshold
					// percentage
					// value
					System.out.println("Match found!");

					// Text to Speech
					speechThread = new SpeechThread(textToSpeech, "Welcome " + txtUserName.getText() + "!");
					speechThread.start();

					// Write property file for User login session
					ConstantsClass.propertyFileHandler.writePropertyFile("UserIDLoggedIn",
							comboUserID.getSelectedItem().toString());

					JOptionPane.showMessageDialog(null, "Welcome " + txtUserName.getText() + "!", "Information Message",
							JOptionPane.INFORMATION_MESSAGE);

					// remove current window from MainFrame's desktop
					jdpMainFrame.removeAll();
				} else {
					// Text to Speech
					speechThread = new SpeechThread(textToSpeech, "Access denied!");
					speechThread.start();

					System.out.println("Mismatch!");
					JOptionPane.showMessageDialog(null, "Access denied!", "Error Message", JOptionPane.ERROR_MESSAGE);

					txtPassword.requestFocus();
				}

				txtPassword.setText("");
			}
		});

		// Adding components
		contentPane.add(lblUserID);
		contentPane.add(comboUserID);
		contentPane.add(lblUserName);
		contentPane.add(txtUserName);
		contentPane.add(lblPassword);
		contentPane.add(txtPassword);
		contentPane.add(btnClear);
		contentPane.add(btnLogin);

		// Panel properties
		setLayout(new BorderLayout());

		add(contentPane, BorderLayout.CENTER);
		// addKeyListener(this);
		setVisible(true);

		// Fill UserID ComboBox
		fillComboUserID();
	}

	private void fillComboUserID() {
		rsUserID = ConstantsClass.dbManager.getDataFromDB("SELECT DISTINCT User_ID FROM Tbl_UserLoginData");

		if (rsUserID != null)
			try {
				while (rsUserID.next()) {
					comboUserID.addItem(rsUserID.getString("User_ID"));
				}

			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
			}
	}

	private void comboUserIDListener() {
		rsUserData = ConstantsClass.dbManager.getDataFromDB(
				"SELECT DISTINCT LoginData.User_ID, LoginData.User_Name, LoginData.Password_Hashed, TimeSeriesData.Time_Series_Text FROM Tbl_UserLoginData LoginData JOIN "
						+ (ConstantsClass.propertyFileHandler.readPropertyFile("TechniqueUsed").equals("Avg")
								? "Tbl_UserKeyTimeSeriesDataAvg"
								: "Tbl_UserKeyTimeSeriesDataMin")
						+ " TimeSeriesData ON LoginData.User_ID = TimeSeriesData.User_ID WHERE LoginData.User_ID = '"
						+ comboUserID.getSelectedItem().toString() + "'");

		if (rsUserData != null)
			try {
				while (rsUserData.next()) {
					txtUserName.setText(rsUserData.getString("User_Name"));
					strPassword = rsUserData.getString("Password_Hashed");
					strUserPasswordPattern = rsUserData.getString("Time_Series_Text");
				}
			} catch (SQLException e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message", JOptionPane.ERROR_MESSAGE);
			}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyPressedEvent();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyReleaseEvent();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		if (businessFunctions.isValidChar(arg0.getKeyChar()))
			businessFunctions.keyTypedEvent();
	}

}
