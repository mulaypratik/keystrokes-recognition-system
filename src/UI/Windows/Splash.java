package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class Splash extends JWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel pnlContent;
	private JLabel lblStatus, lblImg;
	private Dimension dmScreen;
	private Color clrSplash;
	private String strImgPath;
	private int intDuration, intWidth, intHeight, intX, intY;

	public Splash(int Duration, boolean UseSleep) {
		strImgPath = "res\\Icons\\Biometrics.png";

		intWidth = 220;
		intHeight = 120;

		intDuration = Duration;
		pnlContent = (JPanel) getContentPane();
		pnlContent.setLayout(new BorderLayout());

		lblStatus = new JLabel("Status Message Here", JLabel.CENTER);

		dmScreen = Toolkit.getDefaultToolkit().getScreenSize();

		intX = (dmScreen.width - intWidth) / 2;
		intY = (dmScreen.height - intHeight) / 2;
		setBounds(intX, intY, intWidth, intHeight);

		lblImg = new JLabel(new ImageIcon(strImgPath));
		pnlContent.add(lblImg, BorderLayout.CENTER);
		pnlContent.add(lblStatus, BorderLayout.SOUTH);
		clrSplash = new Color(0, 0, 0, 255);
		pnlContent.setBorder(BorderFactory.createLineBorder(clrSplash, 1));

		if (UseSleep) {
			setVisible(true);
			try {
				Thread.sleep(intDuration);
			} catch (Exception e) {
				e.printStackTrace();
			}

			setVisible(false);
		}
	}

	public void setStatus(String ThisStat) {
		lblStatus.setText(ThisStat);
	}

	public void setVisible(boolean Cond) {
		super.setVisible(Cond);

	}

	public void sleep(int MilliSecs) {
		try {
			Thread.sleep(MilliSecs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
