package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlSetThresholdValue extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JTabbedPane tbpSettings;
	private JPanel pnlThresholdsettings, pnlTechniqueSettings, pnlMasterThresholdValueContainer,
			pnlThresholdValueOverall, pnlButtonsContainerThresholdValues, pnlRbTechniquesContainer,
			pnlButtonsContainerTechniqueSettings, pnlThresholdValueVector;
	private TitledBorder ttlBorderToPnlOverall, ttlBorderToPnlVector;
	private SpinnerNumberModel numThresholdValuePerVector, numThresholdValueOverall;
	private JSpinner spnrThresholdValueOverall, spnrThresholdValuePerVector;
	private JLabel lblThresholdValue;
	private JButton btnSetDefaultThresholdValues, btnSaveThresholdValues, btnSetDefaultExtractionTechnique,
			btnSaveExtractionTechnique;
	private ButtonGroup bgRbTechniqueUsed;
	private JRadioButton rbTechniqueAvg, rbTechniqueMin;
	private JCheckBox chkAdaptDBAverageAndStoreList;

	public PnlSetThresholdValue(final JLabel StatusBar) {
		// TitledBorders
		ttlBorderToPnlOverall = new TitledBorder(BorderFactory.createLineBorder(Color.BLACK), "Threshold Value Overall",
				TitledBorder.CENTER, TitledBorder.ABOVE_TOP, new Font(null, Font.BOLD, 12));

		ttlBorderToPnlVector = new TitledBorder(BorderFactory.createLineBorder(Color.BLACK),
				"Threshold Value Per Vector", TitledBorder.CENTER, TitledBorder.ABOVE_TOP,
				new Font(null, Font.BOLD, 12));

		// TabbedPanes
		tbpSettings = new JTabbedPane();

		// Panels
		pnlThresholdsettings = new JPanel();
		pnlThresholdsettings.setLayout(new BorderLayout());

		pnlTechniqueSettings = new JPanel();
		pnlTechniqueSettings.setLayout(new BorderLayout());

		pnlMasterThresholdValueContainer = new JPanel();
		pnlMasterThresholdValueContainer.setLayout(new GridLayout(1, 2, 2, 2));

		pnlRbTechniquesContainer = new JPanel();
		pnlRbTechniquesContainer.setLayout(new GridLayout(1, 2, 2, 2));
		pnlRbTechniquesContainer.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		pnlThresholdValueOverall = new JPanel();
		pnlThresholdValueOverall.setBorder(ttlBorderToPnlOverall);

		pnlThresholdValueVector = new JPanel();
		pnlThresholdValueVector.setBorder(ttlBorderToPnlVector);

		pnlButtonsContainerThresholdValues = new JPanel();
		pnlButtonsContainerThresholdValues.setLayout(new GridLayout(1, 2, 2, 2));

		pnlButtonsContainerTechniqueSettings = new JPanel();
		pnlButtonsContainerTechniqueSettings.setLayout(new GridLayout(1, 2, 2, 2));

		// Labels
		lblThresholdValue = new JLabel("  Set Biometrics accuracy Threshold percentage value:");

		// NumberModel
		numThresholdValuePerVector = new SpinnerNumberModel(
				Integer.parseInt(ConstantsClass.propertyFileHandler.readPropertyFile("ThresholdPercentagePerVector")),
				50, 100, 5);

		numThresholdValueOverall = new SpinnerNumberModel(
				Integer.parseInt(ConstantsClass.propertyFileHandler.readPropertyFile("ThresholdPercentageOverall")), 50,
				100, 5);

		// Spinners
		spnrThresholdValuePerVector = new JSpinner(numThresholdValuePerVector);
		spnrThresholdValuePerVector.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				btnSaveThresholdValues.setEnabled(true);
			}
		});

		spnrThresholdValueOverall = new JSpinner(numThresholdValueOverall);
		spnrThresholdValueOverall.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				btnSaveThresholdValues.setEnabled(true);
			}
		});

		// Buttons
		btnSetDefaultThresholdValues = new JButton("Set Default Values");
		btnSetDefaultThresholdValues.setIcon(new ImageIcon("res\\Icons\\Default.png"));
		btnSetDefaultThresholdValues.setMnemonic('D');
		btnSetDefaultThresholdValues.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnSaveThresholdValues.setEnabled(true);

				numThresholdValuePerVector.setValue(65);
				numThresholdValueOverall.setValue(75);
			}
		});

		btnSaveThresholdValues = new JButton("Save Changes");
		btnSaveThresholdValues.setIcon(new ImageIcon("res\\Icons\\Save.png"));
		btnSaveThresholdValues.setMnemonic('S');
		btnSaveThresholdValues.setEnabled(false);
		btnSaveThresholdValues.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnSaveThresholdValues.setEnabled(false);

				ConstantsClass.propertyFileHandler.writePropertyFile("ThresholdPercentagePerVector",
						spnrThresholdValuePerVector.getValue().toString());

				ConstantsClass.propertyFileHandler.writePropertyFile("ThresholdPercentageOverall",
						spnrThresholdValueOverall.getValue().toString());

				StatusBar.setText("    Threshold value is set to: " + spnrThresholdValueOverall.getValue().toString());
			}
		});

		btnSetDefaultExtractionTechnique = new JButton("Set Default Settings");
		btnSetDefaultExtractionTechnique.setIcon(new ImageIcon("res\\Icons\\Default.png"));
		btnSetDefaultExtractionTechnique.setMnemonic('D');
		btnSetDefaultExtractionTechnique.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				rbTechniqueAvg.setSelected(true);

				chkAdaptDBAverageAndStoreList.setSelected(false);

				btnSaveExtractionTechnique.setEnabled(true);
			}
		});

		btnSaveExtractionTechnique = new JButton("Save Changes");
		btnSaveExtractionTechnique.setIcon(new ImageIcon("res\\Icons\\Save.png"));
		btnSaveExtractionTechnique.setMnemonic('S');
		btnSaveExtractionTechnique.setEnabled(false);
		btnSaveExtractionTechnique.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnSaveExtractionTechnique.setEnabled(false);

				ConstantsClass.propertyFileHandler.writePropertyFile("TechniqueUsed",
						(rbTechniqueAvg.isSelected() ? "Avg" : "Min"));

				ConstantsClass.propertyFileHandler.writePropertyFile("AdaptDBAverageAndStore",
						(chkAdaptDBAverageAndStoreList.isSelected() ? "true" : "false"));
			}
		});

		// ButtonGroups
		bgRbTechniqueUsed = new ButtonGroup();

		// RadioButtons
		rbTechniqueAvg = new JRadioButton("Average Vector");
		rbTechniqueAvg.setSelected(ConstantsClass.propertyFileHandler.readPropertyFile("TechniqueUsed").equals("Avg"));
		rbTechniqueAvg.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				btnSaveExtractionTechnique.setEnabled(true);
			}
		});

		rbTechniqueMin = new JRadioButton("Minimum Vector");
		rbTechniqueMin.setSelected(!rbTechniqueAvg.isSelected());
		rbTechniqueMin.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				btnSaveExtractionTechnique.setEnabled(true);
			}
		});

		// CheckBoxes
		chkAdaptDBAverageAndStoreList = new JCheckBox("Adapt database by storing average of current and stored vector");
		chkAdaptDBAverageAndStoreList.setSelected(
				ConstantsClass.propertyFileHandler.readPropertyFile("AdaptDBAverageAndStore").equals("true"));
		chkAdaptDBAverageAndStoreList.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {
				btnSaveExtractionTechnique.setEnabled(true);
			}
		});

		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout());

		// Adding components
		bgRbTechniqueUsed.add(rbTechniqueAvg);
		bgRbTechniqueUsed.add(rbTechniqueMin);

		pnlThresholdValueVector.add(spnrThresholdValuePerVector);
		pnlThresholdValueOverall.add(spnrThresholdValueOverall);

		pnlMasterThresholdValueContainer.add(pnlThresholdValueVector);
		pnlMasterThresholdValueContainer.add(pnlThresholdValueOverall);

		pnlRbTechniquesContainer.add(rbTechniqueAvg);
		pnlRbTechniquesContainer.add(rbTechniqueMin);

		pnlButtonsContainerThresholdValues.add(btnSetDefaultThresholdValues);
		pnlButtonsContainerThresholdValues.add(btnSaveThresholdValues);

		pnlButtonsContainerTechniqueSettings.add(btnSetDefaultExtractionTechnique);
		pnlButtonsContainerTechniqueSettings.add(btnSaveExtractionTechnique);

		pnlThresholdsettings.add(lblThresholdValue, BorderLayout.NORTH);
		pnlThresholdsettings.add(pnlMasterThresholdValueContainer, BorderLayout.CENTER);
		pnlThresholdsettings.add(pnlButtonsContainerThresholdValues, BorderLayout.SOUTH);

		pnlTechniqueSettings.add(pnlRbTechniquesContainer, BorderLayout.NORTH);
		pnlTechniqueSettings.add(chkAdaptDBAverageAndStoreList, BorderLayout.CENTER);
		pnlTechniqueSettings.add(pnlButtonsContainerTechniqueSettings, BorderLayout.SOUTH);

		tbpSettings.add(pnlThresholdsettings, "Threshold Settings");
		tbpSettings.add(pnlTechniqueSettings, "Feature Extraction Technique Settings");

		add(tbpSettings, BorderLayout.CENTER);

		// Panel properties
		setVisible(true);
	}

}
