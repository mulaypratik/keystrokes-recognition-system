package UI.Windows;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import DB.DBManager;
import DB.ThreadDBConnector;
import BL.Encryption.CaesarCypher;
import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PnlDBServerSettings extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel lblServerIP, lblUserName, lblPswd;
	private JTextField txtServerIP, txtUserName;
	private JPasswordField txtPswd;
	private JButton btnDisconnect, btnConnect;
	private JCheckBox chkSaveDetails;
	private JPanel pnlPswAndSave, pnlContainer;

	private String strDBName = "";

	private CaesarCypher caesarCypher = null;
	private ThreadDBConnector threadDBConnector = null;

	public PnlDBServerSettings(String DBName, final JLabel StatusBar) {
		// Local variables initialization
		strDBName = DBName;

		// Initialize other class objects
		caesarCypher = new CaesarCypher();

		// Panels
		pnlContainer = new JPanel();
		pnlContainer.setLayout(new GridLayout(4, 2));

		pnlPswAndSave = new JPanel();
		pnlPswAndSave.setLayout(new GridLayout(1, 2));

		// Labels
		lblServerIP = new JLabel("    IP Address:");
		lblServerIP.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblServerIP.setLabelFor(txtServerIP);

		lblUserName = new JLabel("    Username:");
		lblUserName.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblUserName.setLabelFor(txtUserName);

		lblPswd = new JLabel("    Password:");
		lblPswd.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPswd.setLabelFor(pnlPswAndSave);

		// TextFields
		txtServerIP = new JTextField();
		txtUserName = new JTextField();
		txtUserName.setText(ConstantsClass.propertyFileHandler.readPropertyFile("DBUserName"));
		txtPswd = new JPasswordField();

		txtPswd.setText(
				caesarCypher.decryptCaesar(ConstantsClass.propertyFileHandler.readPropertyFile("DBPassword"), 3));

		txtServerIP.setText(ConstantsClass.propertyFileHandler.readPropertyFile("DBServerIP"));
		txtServerIP.setFont(new Font("Tahoma", Font.PLAIN, 15));
		txtServerIP.setColumns(10);
		txtServerIP.requestFocusInWindow();

		// CheckBoxes
		chkSaveDetails = new JCheckBox("Save Server Details");
		chkSaveDetails.setSelected(true);
		chkSaveDetails.setFont(new Font("Tahoma", Font.BOLD, 15));

		// Buttons
		btnConnect = new JButton("Connect");
		btnConnect.setIcon(new ImageIcon("res\\Icons\\Connect.png"));
		btnConnect.setToolTipText("Connect to Database Server");
		btnConnect.setMnemonic('C');
		btnConnect.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnConnect.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {

				if (txtServerIP.getText().trim().length() == 0) {
					JOptionPane.showMessageDialog(null, "Please enter a valid Server's IP.", "Error Message",
							JOptionPane.ERROR_MESSAGE);
				} else {

					threadDBConnector = new ThreadDBConnector(new DBManager(txtServerIP.getText(),
							ConstantsClass.propertyFileHandler.readPropertyFile("DBPort"), txtUserName.getText(),
							txtPswd.getText(), strDBName), StatusBar);
					threadDBConnector.start();

					// Make current thread wait
					synchronized (threadDBConnector) {
						try {
							threadDBConnector.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(null, e.getMessage(), "Error Message",
									JOptionPane.ERROR_MESSAGE);
						}
					}

					// Write Server credentials to Property file
					writePropertyFile();

					setComponentsStateOnConnection(false);
				}
			}
		});

		btnDisconnect = new JButton("Disconnect");
		btnDisconnect.setIcon(new ImageIcon("res\\Icons\\Disconnect.png"));
		btnDisconnect.setToolTipText("Disconnect from Database Server");
		btnDisconnect.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnDisconnect.setMnemonic('D');
		btnDisconnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (ConstantsClass.dbManager != null)
					ConstantsClass.dbManager.closeConnection();

				ConstantsClass.dbManager = null;

				StatusBar.setText("    Connection Desconnected!");

				setComponentsStateOnConnection(true);
			}
		});

		// Adding components
		pnlPswAndSave.add(txtPswd);
		pnlPswAndSave.add(chkSaveDetails);

		pnlContainer.add(lblServerIP);
		pnlContainer.add(txtServerIP);
		pnlContainer.add(lblUserName);
		pnlContainer.add(txtUserName);
		pnlContainer.add(lblPswd);
		pnlContainer.add(pnlPswAndSave);
		pnlContainer.add(btnDisconnect);
		pnlContainer.add(btnConnect);

		// Panel properties
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setLayout(new BorderLayout());

		add(pnlContainer, BorderLayout.CENTER);

		setVisible(true);

		// Set Components State according to DB connectivity
		if (ConstantsClass.dbManager != null)
			setComponentsStateOnConnection(false);
		else
			setComponentsStateOnConnection(true);
	}

	private void setComponentsStateOnConnection(boolean TrueOrFalse) {
		txtServerIP.setEnabled(TrueOrFalse);
		txtUserName.setEnabled(TrueOrFalse);
		txtPswd.setEnabled(TrueOrFalse);
		btnConnect.setEnabled(TrueOrFalse);
		btnDisconnect.setEnabled(!TrueOrFalse);
		chkSaveDetails.setEnabled(TrueOrFalse);
		btnConnect.setEnabled(TrueOrFalse);

		txtServerIP.setText(ConstantsClass.propertyFileHandler.readPropertyFile("DBServerIP"));
		txtUserName.setText(ConstantsClass.propertyFileHandler.readPropertyFile("DBUserName"));
		txtPswd.setText(
				caesarCypher.decryptCaesar(ConstantsClass.propertyFileHandler.readPropertyFile("DBPassword"), 3));
	}

	@SuppressWarnings("deprecation")
	private void writePropertyFile() {
		if (chkSaveDetails.isSelected()) {
			ConstantsClass.propertyFileHandler.writePropertyFile("DBServerIP", txtServerIP.getText());

			ConstantsClass.propertyFileHandler.writePropertyFile("DBUserName", txtUserName.getText());

			ConstantsClass.propertyFileHandler.writePropertyFile("DBPassword",
					caesarCypher.applyCaesar(txtPswd.getText(), 3));
		}
	}

}
