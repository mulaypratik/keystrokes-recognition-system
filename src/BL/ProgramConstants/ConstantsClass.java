/**
 * 
 */
package BL.ProgramConstants;

import BL.ServerDetailsSave.PropertyFileHandler;
import DB.DBManager;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public final class ConstantsClass {

	public static DBManager dbManager = null;
	public static PropertyFileHandler propertyFileHandler = null;

}
