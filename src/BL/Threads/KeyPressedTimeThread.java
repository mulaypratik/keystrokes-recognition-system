package BL.Threads;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class KeyPressedTimeThread extends Thread {

	public boolean boolRunThread = true;
	private int intI = 0;

	public KeyPressedTimeThread() {
		setPriority(MAX_PRIORITY);
	}

	public void run() {
		while (boolRunThread) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			intI++;
		}
	}

	public int getI() {
		boolRunThread = false;
		// System.out.println("KP Thread stopped!");
		return intI;
	}

}
