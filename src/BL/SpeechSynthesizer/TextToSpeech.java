package BL.SpeechSynthesizer;

import java.util.Locale;

import javax.speech.Central;
import javax.speech.EngineException;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class TextToSpeech {

	private Synthesizer synthesizer = null;

	public TextToSpeech() {
		System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");

		try {
			Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");

			synthesizer = Central.createSynthesizer(new SynthesizerModeDesc(Locale.US));
		} catch (EngineException e) {
			e.printStackTrace();
		}
	}

	public void speak(String TextToBeSpoken) {
		try {
			synthesizer.allocate();
			synthesizer.resume();
			synthesizer.speakPlainText(TextToBeSpoken, null);
			synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
			// synthesizer.deallocate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static void main(String[] args) { TextToSpeech tt = new
	 * TextToSpeech(); //tt.speak("Ganapati bappa!");
	 * tt.speak("Welcome Pratik Mulay!"); //
	 * tt.speak("Welcome to Hi-tech Biometrics System!"); }
	 */
}
