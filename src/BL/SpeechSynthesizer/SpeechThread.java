package BL.SpeechSynthesizer;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class SpeechThread extends Thread {

	private String textToBeSpoken = "";

	private TextToSpeech textToSpeech = null;

	public SpeechThread(TextToSpeech TtoSpeech, String TToBeSpoken) {
		textToSpeech = TtoSpeech;

		textToBeSpoken = TToBeSpoken;
	}

	@Override
	public void run() {
		textToSpeech.speak(textToBeSpoken);
	}

}
