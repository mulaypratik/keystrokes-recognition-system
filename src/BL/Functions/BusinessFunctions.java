package BL.Functions;

import java.util.ArrayList;

import javax.swing.JLabel;

import BL.ProgramConstants.ConstantsClass;
import BL.ServerDetailsSave.PropertyFileHandler;
import BL.Threads.KeyPressedTimeThread;
import BL.Threads.KeyTypedTimeThread;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class BusinessFunctions {

	private ArrayList<Integer> arlUserInputPattern;
	private static ArrayList<Integer> arlUserPatternSample1, arlUserPatternSample2, arlUserPatternSample3;
	private boolean boolPressed = false, boolTyped = false;
	private int intPercentageMatch = 0, intCountRecords = 0;
	private String strErrMsg = "", strRole = "";

	private JLabel lblStatusBar;

	private KeyPressedTimeThread timeThreadKP = null;
	private KeyTypedTimeThread timeThreadKT = null;
	private PropertyFileHandler propertyFileHandler = null;

	public BusinessFunctions(JLabel StatusBar) {
		arlUserInputPattern = new ArrayList<>();

		lblStatusBar = StatusBar;

		propertyFileHandler = new PropertyFileHandler("config");
	}

	private static int percentageMatch(float FirstNo, float SecondNo) {
		if (FirstNo == 0 && SecondNo == 0)
			return 0;

		if ((FirstNo == 0 && SecondNo != 0) || (FirstNo != 0 && SecondNo == 0))
			return 100;

		if (FirstNo < SecondNo)
			return (int) ((FirstNo / SecondNo) * 100);
		else
			return (int) ((SecondNo / FirstNo) * 100);
	}

	private boolean comparePatternLists(ArrayList<Integer> arlListToBeCompared, ArrayList<Integer> arlBaseList,
			int ComparisonThresholdPerVector, int ComparisonThresholdOverall) {
		ArrayList<Boolean> arlBoolean = new ArrayList<>();
		int intNoOfTrue = 0, intNoOfFalse = 0;

		System.out.println("#arlBaseList: " + arlBaseList.size());
		System.out.println("#arlListToBeCompared: " + arlListToBeCompared.size());

		if (arlBaseList.size() != arlListToBeCompared.size()) {
			if (lblStatusBar != null)
				lblStatusBar.setText("    Keystroke dynamics match: " + 0 + "%");

			return false;
		}

		for (int i = 0; i < arlBaseList.size(); i++) {
			if (percentageMatch(arlListToBeCompared.get(i), arlBaseList.get(i)) < ComparisonThresholdPerVector)
				arlBoolean.add(false);
			else
				arlBoolean.add(true);
		}

		for (int i = 0; i < arlBoolean.size(); i++) {
			if (arlBoolean.get(i) == true)
				intNoOfTrue++;
			else
				intNoOfFalse++;
		}

		System.out.println("#True: " + intNoOfTrue);
		System.out.println("#False: " + intNoOfFalse);

		if (intNoOfTrue == 0) {
			if (lblStatusBar != null)
				lblStatusBar.setText("    Keystroke dynamics match: " + 0 + "%");
			return false;
		}

		intPercentageMatch = percentageMatch(intNoOfTrue, (intNoOfTrue + intNoOfFalse));
		System.out.println("Percentage match: " + intPercentageMatch);

		if (lblStatusBar != null)
			lblStatusBar.setText("    Keystroke dynamics match: " + intPercentageMatch + "%");

		if (intPercentageMatch < ComparisonThresholdOverall)
			return false;

		return true;
	}

	public boolean PasswordMatch(String UserInput, String Password, String PasswordPattern,
			int ThresholdPercentagePerVector, int ThresholdPercentageOverall) {
		boolean isMatched = false;

		timeThreadKP = null;
		timeThreadKT = null;

		if (UserInput.equals(Password)) {
			if (comparePatternLists(arlUserInputPattern, getArListFromStringPattern(PasswordPattern),
					ThresholdPercentagePerVector, ThresholdPercentageOverall))
				isMatched = true;
			else
				isMatched = false;
		} else {
			isMatched = false;
			if (lblStatusBar != null)
				lblStatusBar.setText("    Keystroke dynamics match: " + 0 + "%");
		}

		clearPatternList();

		return isMatched;
	}

	public void clearPatternList() {
		timeThreadKP = null;
		timeThreadKT = null;

		arlUserInputPattern.clear();
	}

	private void clearThreeInputList() {
		timeThreadKP = null;
		timeThreadKT = null;

		arlUserPatternSample1.clear();
		arlUserPatternSample2.clear();
		arlUserPatternSample3.clear();
	}

	private String patternLengthPadder(int Number) {
		String str0PaddedNo = "";

		switch ((Number + "").length()) {
		case 1:
			str0PaddedNo = "000" + Number;
			break;
		case 2:
			str0PaddedNo = "00" + Number;
			break;
		case 3:
			str0PaddedNo = "0" + Number;
			break;
		default:
			str0PaddedNo = "" + Number;
			break;
		}

		return str0PaddedNo;
	}

	private ArrayList<Integer> getArListFromStringPattern(String StringPattern) {
		ArrayList<Integer> arlComparisonPattern = new ArrayList<>();

		for (int start = 0; start < StringPattern.length(); start += 4) {
			arlComparisonPattern
					.add(Integer.parseInt(StringPattern.substring(start, Math.min(StringPattern.length(), start + 4))));
		}

		for (int i = 0; i < arlComparisonPattern.size(); i++)
			System.out.println("Recorded pattern: " + arlComparisonPattern.get(i));

		return arlComparisonPattern;
	}

	private String patternToBeStoredAsString(ArrayList<Integer> UserPattern) {
		String strPatternToBeStoredInDB = "";

		for (int i = 0; i < UserPattern.size(); i++) {
			strPatternToBeStoredInDB += patternLengthPadder(UserPattern.get(i));
		}

		return strPatternToBeStoredInDB;
	}

	public void getPatternSampleFromUser(int NoOfTime) {
		switch (NoOfTime) {
		case 1:
			arlUserPatternSample1 = new ArrayList<>(arlUserInputPattern);
			break;
		case 2:
			arlUserPatternSample2 = new ArrayList<>(arlUserInputPattern);
			break;
		case 3:
			arlUserPatternSample3 = new ArrayList<>(arlUserInputPattern);
			break;
		}

		System.out.println("List1: " + arlUserPatternSample1);
		System.out.println("List2: " + arlUserPatternSample2);
		System.out.println("List3: " + arlUserPatternSample3);

		clearPatternList();
	}

	private ArrayList<Integer> calculateAvgList(ArrayList<Integer> UserPatternSample1,
			ArrayList<Integer> UserPatternSample2, ArrayList<Integer> UserPatternSample3) {
		if ((UserPatternSample1.size() == UserPatternSample2.size())
				&& (UserPatternSample2.size() == UserPatternSample3.size())) {

			ArrayList<Integer> arlAvgOfUserPatternSamples = new ArrayList<>();

			for (int i = 0; i < UserPatternSample1.size(); i++) {
				arlAvgOfUserPatternSamples
						.add((UserPatternSample1.get(i) + UserPatternSample2.get(i) + UserPatternSample3.get(i)) / 3);
			}

			System.out.println("AvgList: " + arlAvgOfUserPatternSamples);
			return arlAvgOfUserPatternSamples;
		}

		return null;
	}

	private ArrayList<Integer> calculateMinList(ArrayList<Integer> UserPatternSample1,
			ArrayList<Integer> UserPatternSample2, ArrayList<Integer> UserPatternSample3) {
		if ((UserPatternSample1.size() == UserPatternSample2.size())
				&& (UserPatternSample2.size() == UserPatternSample3.size())) {

			ArrayList<Integer> arlMinOfUserPatternSamples = new ArrayList<>();
			ArrayList<Integer> arlMinOfUserPatternSamplesTemp = new ArrayList<>();

			for (int i = 0; i < UserPatternSample1.size(); i++) {
				if (UserPatternSample1.get(i) < UserPatternSample2.get(i))
					arlMinOfUserPatternSamplesTemp.add(UserPatternSample1.get(i));
				else
					arlMinOfUserPatternSamplesTemp.add(UserPatternSample2.get(i));

				if (arlMinOfUserPatternSamplesTemp.get(i) < UserPatternSample3.get(i))
					arlMinOfUserPatternSamples.add(arlMinOfUserPatternSamplesTemp.get(i));
				else
					arlMinOfUserPatternSamples.add(UserPatternSample3.get(i));
			}

			System.out.println("MinList: " + arlMinOfUserPatternSamples);
			return arlMinOfUserPatternSamples;
		}

		return null;
	}

	public String saveUserRegistrationDataInDB(String UserID, String UserName, String UserRole, String Password_Text,
			String Password_Hashed) {
		// DB Insert Queries
		strErrMsg = ConstantsClass.dbManager.executeQuery("INSERT INTO Tbl_UserLoginData VALUES ('" + UserID + "', '"
				+ UserName + "', '" + Password_Hashed + "', '" + UserRole + "', '" + Password_Text + "')");

		// Store AvgList
		if (strErrMsg == null)
			strErrMsg = ConstantsClass.dbManager.executeQuery("INSERT INTO Tbl_UserKeyTimeSeriesDataAvg VALUES ('"
					+ UserID + "', '"
					+ patternToBeStoredAsString(
							calculateAvgList(arlUserPatternSample1, arlUserPatternSample2, arlUserPatternSample3))
					+ "')");

		// Store MinList
		if (strErrMsg == null)
			strErrMsg = ConstantsClass.dbManager.executeQuery("INSERT INTO Tbl_UserKeyTimeSeriesDataMin VALUES ('"
					+ UserID + "', '"
					+ patternToBeStoredAsString(
							calculateMinList(arlUserPatternSample1, arlUserPatternSample2, arlUserPatternSample3))
					+ "')");

		clearPatternList();
		clearThreeInputList();

		return strErrMsg;
	}

	public void keyPressedEvent() {
		if (!boolPressed) {
			timeThreadKP = new KeyPressedTimeThread();
			timeThreadKP.start();

			boolPressed = true;
		}
	}

	public void keyReleaseEvent() {
		if (timeThreadKP != null) {
			arlUserInputPattern.add(timeThreadKP.getI());
			System.out.println(arlUserInputPattern.get(arlUserInputPattern.size() - 1));
			boolPressed = false;
		}
	}

	public void keyTypedEvent() {
		if (!boolTyped) {
			if (timeThreadKT != null) {
				arlUserInputPattern.add(timeThreadKT.getI());
				System.out.println("KeyTyped: " + arlUserInputPattern.get(arlUserInputPattern.size() - 1));
				boolTyped = false;
			}

			timeThreadKT = new KeyTypedTimeThread();
			timeThreadKT.start();

			boolTyped = true;
		} else {

			if (timeThreadKT != null) {
				arlUserInputPattern.add(timeThreadKT.getI());
				System.out.println("KeyTyped: " + arlUserInputPattern.get(arlUserInputPattern.size() - 1));

				boolTyped = false;
			}

			timeThreadKT = new KeyTypedTimeThread();
			timeThreadKT.start();
		}
	}

	public boolean isValidChar(char charToBeChecked) {
		if (Character.isDigit(charToBeChecked))
			return true;

		if (Character.isLetter(charToBeChecked))
			return true;

		return false;
	}

	public boolean isAuthenticUser() {
		if (ConstantsClass.dbManager == null)
			return false;

		// If DB table Tbl_UserLoginData is empty, then allow all settings (for first
		// Admin registration)
		intCountRecords = Integer.parseInt(
				ConstantsClass.dbManager.getStringFromDB("SELECT COUNT(*) AS CountRecords FROM Tbl_UserLoginData;"));

		if (intCountRecords == 0) {
			System.out.println("First time");
			return true;
		}

		strRole = ConstantsClass.dbManager
				.getStringFromDB("SELECT DISTINCT User_Role FROM Tbl_UserLoginData WHERE User_ID = '"
						+ propertyFileHandler.readPropertyFile("UserIDLoggedIn") + "'");

		if (strRole.equals("Admin"))
			return true;
		else
			return false;

//		return true;
	}

	public void logoutUser() {
		// Read property in order to avoid overwriting the file.
		propertyFileHandler.readPropertyFile("UserIDLoggedIn");
		propertyFileHandler.writePropertyFile("UserIDLoggedIn", "");
	}

}
