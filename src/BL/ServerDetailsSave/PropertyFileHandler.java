package BL.ServerDetailsSave;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import BL.ProgramConstants.ConstantsClass;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class PropertyFileHandler {

	private Properties props = null;
	private OutputStream osPropertyFile = null;
	private InputStream isPropertyFile = null;
	private String strPropertyFileName = "";

	public PropertyFileHandler(String PropertyFileName) {
		props = new Properties();
		strPropertyFileName = PropertyFileName;

		ConstantsClass.propertyFileHandler = this;
	}

	public void writePropertyFile(String PropertyName, String PropertyValue) {
		try {
			osPropertyFile = new FileOutputStream(strPropertyFileName + ".properties");
			// set the properties value
			props.setProperty(PropertyName, PropertyValue);
			// save properties to project root folder
			props.store(osPropertyFile, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (osPropertyFile != null) {
				try {
					osPropertyFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public String readPropertyFile(String PropertyName) {
		String strPropertyValue = "";

		try {
			isPropertyFile = new FileInputStream(strPropertyFileName + ".properties");
			// load a properties file
			props.load(isPropertyFile);
			// get the property value and print it out
			strPropertyValue = props.getProperty(PropertyName);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (isPropertyFile != null) {
				try {
					isPropertyFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return strPropertyValue;
	}

}
