package BL.Encryption;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class CaesarCypher {
	/**
	 * @param args
	 */
	public String applyCaesar(String text, int shift) {
		char[] chars = text.toCharArray();
		for (int i = 0; i < text.length(); i++) {
			char c = chars[i];
			if (c >= 32 && c <= 127) {
				int x = c - 32;
				x = (x + shift) % 96;
				if (x < 0)
					x += 96;
				chars[i] = (char) (x + 32);
			}
		}

		return new String(chars);
	}

	public String decryptCaesar(String text, int shift) {
		char[] chars = text.toCharArray();
		for (int i = 0; i < text.length(); i++) {
			char c = chars[i];
			if (c >= 32 && c <= 127) {
				int x = c - 32;
				x = (x - shift) % 96;
				if (x < 0)
					x += 96;
				chars[i] = (char) (x + 32);
			}
		}

		return new String(chars);
	}

}
