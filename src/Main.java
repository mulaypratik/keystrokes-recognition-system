import UI.Windows.Splash;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import UI.Windows.MainFrame;

/**
 * @author Pratik C. Mulay
 *
 *         (mulaypratik30@gmail.com)
 *
 *         23-Apr-2015
 */
public class Main {
	/**
	 * @param args
	 */

	public static Splash mySplash;

	public static void main(String[] args) {

		mySplash = new Splash(5000, false);
		mySplash.setVisible(true);
		mySplash.setStatus("Loading Keystrokes Reco. System...");
		mySplash.sleep(2000);

		mySplash.setStatus("Load completed.");
		mySplash.sleep(1000);
		mySplash.setVisible(false);

		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		new MainFrame();
	}

}
